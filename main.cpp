#include <QCoreApplication>
#include "IProperty.h"
#include <iostream>

#include "loghelper.h"
#include "classwithproperties.h"
#include "propertyclass.h"

using namespace std;

#ifndef IPROPERTYEXAMPLE_H
#define IPROPERTYEXAMPLE_H

class IPropertyExample
{
public:
    static void showExample() {
        ClassWithProperties* instanceOf_ClassWithProperties1 = new ClassWithProperties();

        logMessage(instanceOf_ClassWithProperties1->TestReadOnlyProperty->Get(),"firstinstance->TestReadOnlyProperty");
        logMessage(instanceOf_ClassWithProperties1->TestCustomProperty1->Get(),"firstinstance->TestCustomProperty1");
        logMessage(instanceOf_ClassWithProperties1->TestCustomProperty2->Get(),"firstinstance->TestCustomProperty2");
        logMessage(instanceOf_ClassWithProperties1->TestLambdaProperty->Get(),"firstinstance->TestLambdaProperty");
        logMessage("===================================");
        //instanceOf_ClassWithProperties1->TestReadOnlyProperty->Set("Overwritten"); //Not possible cause RO
        instanceOf_ClassWithProperties1->TestCustomProperty1->Set("*");
        instanceOf_ClassWithProperties1->TestCustomProperty2->Set("2");
        instanceOf_ClassWithProperties1->TestLambdaProperty->Set("Changed");
        logMessage(instanceOf_ClassWithProperties1->TestReadOnlyProperty->Get(),"firstinstance->TestReadOnlyProperty");
        logMessage(instanceOf_ClassWithProperties1->TestCustomProperty1->Get(),"firstinstance->TestCustomProperty1");
        logMessage(instanceOf_ClassWithProperties1->TestCustomProperty2->Get(),"firstinstance->TestCustomProperty2");
        logMessage(instanceOf_ClassWithProperties1->TestLambdaProperty->Get(),"firstinstance->TestLambdaProperty");

        ClassWithProperties* instanceOf_ClassWithProperties2 = new ClassWithProperties();

        instanceOf_ClassWithProperties2->TestCustomProperty1->Set("?");
        instanceOf_ClassWithProperties2->TestCustomProperty2->Set("Y");
        logMessage(instanceOf_ClassWithProperties2->TestCustomProperty1->Get(),"secondinstance->TestCustomProperty1");
        logMessage(instanceOf_ClassWithProperties2->TestCustomProperty2->Get(),"secondinstance->TestCustomProperty2");
        logMessage(instanceOf_ClassWithProperties2->TestReadOnlyIndependentProperty->Get(),"secondinstance->TestReadOnlyIndependentProperty");
        logMessage(instanceOf_ClassWithProperties2->TestROLambdaProperty->Get(),"secondinstance->TestROLambdaProperty");

        logMessage("===================================");
        instanceOf_ClassWithProperties1->TestClassProperty->Set(new PropertyClass("Hello World"));
        logMessage(instanceOf_ClassWithProperties1->TestClassProperty->Get()->strPropertyClassValue,"firstinstance->TestClassProperty->strPropertyClassValue");
        logMessage(instanceOf_ClassWithProperties2->TestClassReadOnlyProperty->Get()->strPropertyClassValue,"secondinstance->TestROClassProperty->strPropertyClassValue");
    }
};

#endif

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    IPropertyExample::showExample();

    return a.exec();
}
