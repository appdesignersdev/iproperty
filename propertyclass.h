#include <stddef.h>

#ifndef PROPERTYCLASS_H
#define PROPERTYCLASS_H

class PropertyClass {
    public:
        std::string strPropertyClassValue = "Hello";
        PropertyClass() { logMessage("New Instance of PropertyClass is created!"); }
        PropertyClass(std::string strValue) : PropertyClass() { strPropertyClassValue = strValue; }
};

#endif // PROPERTYCLASS_H
