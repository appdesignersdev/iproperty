/*
 * IProperty.h gives C++ Property capabilities (For Usage see Example below class)
 *
 * Copyright (C) 2014 Aamer Q. Mahmood (support@appdesigners.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "INullable.h"
#include <iostream>
#include <functional>
#include <stddef.h>

using namespace std;

#ifndef IProperty_h
#define IProperty_h

template<class T, class Delegate>
class IBaseProperty {

protected:
    INullable<T>* _propertyNullableValue = new INullable<T>();
    Delegate *_delegate;
    T (Delegate::*_pointerGetterFnk)();
    void (Delegate::*_pointerSetterFnk)(T);

    #if __cplusplus > 199711L
        function<T()> _lambdaGetterFnk;
        function<void(T)> _lambdaSetterFnk;
    #endif

    bool _boolHasSetter = false;
    bool _boolIndependentProperty = false;
    bool _boolLambdaProperty = false;

    void shadowSet(T propertyValue) {
        delete _propertyNullableValue;
        _propertyNullableValue = new INullable<T>(propertyValue);
        if (_boolHasSetter) {
            #if __cplusplus > 199711L
            if (_boolLambdaProperty) _lambdaSetterFnk(propertyValue);
                                else
            #endif
                                    (_delegate->*_pointerSetterFnk)(propertyValue);
        } else _propertyNullableValue->setValue(propertyValue);
    }

    IBaseProperty () { }

    ~IBaseProperty () {
        delete _propertyNullableValue;
    }

public:

    T Get( ) {
        if (_boolLambdaProperty) {
            #if __cplusplus > 199711L
                _propertyNullableValue->Value = _lambdaGetterFnk();
            #endif
        } else {
            if (_propertyNullableValue->IsEmpty)
                shadowSet((_delegate->*_pointerGetterFnk)());
            else { if (!_boolIndependentProperty)
                        _propertyNullableValue->Value = (_delegate->*_pointerGetterFnk)(); }
        }
        return _propertyNullableValue->Value;
    }

    inline bool IsEmpty() { return _propertyNullableValue->IsEmpty; }

    void injectDelegate(Delegate *delegate) {
        this->_delegate = delegate;
    }

    IBaseProperty (T propertyValue) {
        _boolHasSetter = false;
        _boolIndependentProperty = true;
        shadowSet(propertyValue);
    }
};

template<class Property>
class IStaticPropertyExtension {
protected:
    Property* _property = NULL;
public:
    IStaticPropertyExtension(Property* property) {
        _property = property;
    }

    IStaticPropertyExtension() {}

    ~IStaticPropertyExtension() {
        if (_property!=NULL) delete _property;
    }
};

template<class T, class Delegate>
class IReadOnlyProperty : public IBaseProperty<T,Delegate>, public IStaticPropertyExtension<IReadOnlyProperty<T,Delegate> > {

public:

    IReadOnlyProperty (T propertyValue) : IBaseProperty<T,Delegate>(propertyValue) {}

    #if __cplusplus > 199711L
    IReadOnlyProperty (function<T()> lambdaGetterFnk) : IBaseProperty<T,Delegate>() {
        this->_lambdaGetterFnk = lambdaGetterFnk;
        this->_lambdaSetterFnk = NULL;
        this->_boolHasSetter = false;
        this->_boolLambdaProperty = true;
    }
    #endif

    T Get() {
        return (this->_property!=NULL ? this->_property->Get() : IBaseProperty<T,Delegate>::Get());
    }
    
    IReadOnlyProperty (T(Delegate::*pointerGetterFnk)()) {
        this->construct(pointerGetterFnk);
    }

    IReadOnlyProperty (Delegate *delegate, T(Delegate::*pointerGetterFnk)()) {
        this->_delegate = delegate;
        this->construct(pointerGetterFnk);
    }

    operator T() {
        return (this->_property!=NULL ? this->_property->Get() : this->Get());
    }

    IReadOnlyProperty& operator=(IReadOnlyProperty<T,Delegate>* property) {
        this->_property = property;
        return *this;
    }
    IReadOnlyProperty(IReadOnlyProperty<T,Delegate>* property) : IStaticPropertyExtension<IReadOnlyProperty<T,Delegate> >(property) {}
protected:
    void construct (T(Delegate::*pointerGetterFnk)()) {
        this->_pointerGetterFnk = pointerGetterFnk;
        this->_pointerSetterFnk = NULL;
        this->_boolHasSetter = false;
    }
};

template<class T, class Delegate>
class IROProperty : public IReadOnlyProperty<T,Delegate> {
public:
    IROProperty (T(Delegate::*pointerGetterFnk)()) { this->construct(pointerGetterFnk); }
    IROProperty (Delegate *delegate, T(Delegate::*pointerGetterFnk)()) : IReadOnlyProperty<T,Delegate>(delegate,pointerGetterFnk){}

    operator T() {
        return (this->_property!=NULL ? this->_property->Get() : this->Get());
    }

    IROProperty& operator=(IROProperty<T,Delegate>* property) {
        this->_property = property;
        return *this;
    }
    IROProperty(IROProperty<T,Delegate>* property) : IReadOnlyProperty<T,Delegate>(property) {}
};

template<class T, class Delegate>
class IROPropertyWithInternalSetter : public IBaseProperty<T,Delegate> {

public:

    #if __cplusplus > 199711L
    IROPropertyWithInternalSetter (function<T()> lambdaGetterFnk, function<void(T)> lambdaSetterFnk) : IBaseProperty<T,Delegate>() {
        this->construct(lambdaGetterFnk,lambdaSetterFnk);
    }
    #endif

    IROPropertyWithInternalSetter (T(Delegate::*pointerGetterFnk)(), void (Delegate::*pointerSetterFnk)(T)){
        this->construct(pointerGetterFnk,pointerSetterFnk);
    }

    IROPropertyWithInternalSetter (Delegate *delegate, T(Delegate::*pointerGetterFnk)(), void (Delegate::*pointerSetterFnk)(T)) {
        this->_delegate = delegate;
        this->construct(pointerGetterFnk,pointerSetterFnk);
    }

    operator T() {
        return this->Get();
    }

protected:
    IROPropertyWithInternalSetter() {}

    void construct(T(Delegate::*pointerGetterFnk)(), void (Delegate::*pointerSetterFnk)(T)) {
        this->_pointerGetterFnk = pointerGetterFnk;
        this->_pointerSetterFnk = pointerSetterFnk;
        this->_boolHasSetter = true;
    }
    #if __cplusplus > 199711L
    void construct(function<T()> lambdaGetterFnk, function<void(T)> lambdaSetterFnk) {
        this->_lambdaGetterFnk = lambdaGetterFnk;
        this->_lambdaSetterFnk = lambdaSetterFnk;
        this->_boolHasSetter = true;
        this->_boolLambdaProperty = true;
    }
    #endif
};

template<class T, class Delegate>
class IProperty : public IROPropertyWithInternalSetter<T,Delegate>, public IStaticPropertyExtension<IProperty<T,Delegate> > {

public:
    void Set(T propertyValue) {
        if (this->_boolHasSetter) this->shadowSet(propertyValue);
    }
    #if __cplusplus > 199711L
    IProperty (function<T()> lambdaGetterFnk, function<void(T)> lambdaSetterFnk) : IROPropertyWithInternalSetter<T,Delegate>(lambdaGetterFnk,lambdaSetterFnk) {
        this->construct(lambdaGetterFnk,lambdaSetterFnk);
    }
    #endif

    IProperty (T(Delegate::*pointerGetterFnk)(), void (Delegate::*pointerSetterFnk)(T))
        : IROPropertyWithInternalSetter<T,Delegate>(pointerGetterFnk,pointerSetterFnk) {}

    IProperty (Delegate *delegate, T(Delegate::*pointerGetterFnk)(), void (Delegate::*pointerSetterFnk)(T))
        : IROPropertyWithInternalSetter<T,Delegate>(delegate,pointerGetterFnk,pointerSetterFnk) {}

    IProperty& operator=(T propertyValue) {
        if (this->_property!=NULL) this->_property->Set(propertyValue);
                              else this->Set(propertyValue);
        return *this;
    }

    operator T() {
        return (this->_property!=NULL ? this->_property->Get() : this->Get());
    }

    IProperty& operator=(IProperty<T,Delegate>* property) {
        this->_property = property;
        return *this;
    }
    IProperty(IProperty<T,Delegate>* property) : IStaticPropertyExtension<IProperty<T,Delegate> >(property) {}
};

#endif
