#include "IProperty.h"
#include "propertyclass.h"
#include <iostream>
#include <sstream>

#ifndef CLASSWITHPROPERTIES_H
#define CLASSWITHPROPERTIES_H

class ClassWithProperties {
    private:
        //Shortcuts
        typedef IReadOnlyProperty<std::string, ClassWithProperties> StringROProperty;
        typedef IReadOnlyProperty<PropertyClass*, ClassWithProperties> PropertyClassROProperty;
        typedef IProperty<std::string, ClassWithProperties> StringProperty;
        typedef IProperty<PropertyClass*, ClassWithProperties> PropertyClassProperty;
    public:
        // Common usage

        StringROProperty* TestReadOnlyProperty = new StringROProperty(this,&ClassWithProperties::GET_TestCustomProperty);
        StringProperty* TestCustomProperty1 = new StringProperty(this,&ClassWithProperties::GET_TestCustomProperty,&ClassWithProperties::SET_TestCustomProperty);
        StringProperty* TestCustomProperty2 = new StringProperty(this,&ClassWithProperties::GET_TestCustomProperty,&ClassWithProperties::SET_TestCustomProperty);
        PropertyClassROProperty* TestClassReadOnlyProperty = new PropertyClassROProperty(this,&ClassWithProperties::GET_TestClassProperty);
        PropertyClassProperty* TestClassProperty = new PropertyClassProperty(this,&ClassWithProperties::GET_TestClassProperty,&ClassWithProperties::SET_TestClassProperty);

        // Simplest usage

        StringROProperty* TestReadOnlyIndependentProperty = new StringROProperty("Blub");

        // More advanced usage

        StringROProperty* TestROLambdaProperty = new StringROProperty([]() {
                                                                              time_t t = time(0); struct tm * now = localtime( & t );
                                                                              std::stringstream sstrDate;
                                                                              sstrDate << (now->tm_mday) << "." << (now->tm_mon + 1) << "." << (now->tm_year + 1900);
                                                                              return sstrDate.str();
                                                                           });
        StringProperty* TestLambdaProperty = new StringProperty([this]() { return this->_strLambdaPropertyValue; },
                                                                [this](std::string str) { this->_strLambdaPropertyValue = str; });
    protected:
        //GET,SET TestCustomProperty
        std::string GET_TestCustomProperty() { return _strCustomPropertyValue; }
        void SET_TestCustomProperty(std::string str) { _strCustomPropertyValue = str; }
        std::string _strCustomPropertyValue = "Initial";

        //GET,SET TestClassProperty
        PropertyClass* _objPropertyClass = NULL;
        PropertyClass* GET_TestClassProperty() { if (_objPropertyClass==NULL) _objPropertyClass = new PropertyClass(); return _objPropertyClass; }
        void SET_TestClassProperty(PropertyClass* objPropertyClass) { _objPropertyClass = objPropertyClass; }

        //Locals TestLambdaProperty
        std::string _strLambdaPropertyValue = "Initial";
};

#endif // CLASSWITHPROPERTIES_H
