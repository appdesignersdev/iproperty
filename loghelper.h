#include <stddef.h>
#ifdef QT_VERSION
#include <QDebug>
#else
#include <iostream>
#endif
using namespace std;

#ifndef LOGHELPER_H
#define LOGHELPER_H

void logMessage(std::string strMessage, std::string strTitle = "Info") {
    #ifdef QT_VERSION
        qDebug() << QString::fromStdString(strTitle) << ":" << QString::fromStdString(strMessage);
    #else
        cout << strTitle << ":" << strMessage;
    #endif
}

#endif // LOGHELPER_H
