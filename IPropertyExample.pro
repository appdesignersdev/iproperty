QT -= gui

CONFIG += c++11
CONFIG += console
CONFIG -= app_bundle
QMAKE_CFLAGS           += -fno-keep-inline-dllexport -std=c++11

SOURCES += \
        main.cpp

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    INullable.h \
    IProperty.h \
    classwithproperties.h \
    loghelper.h \
    propertyclass.h
